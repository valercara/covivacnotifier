const request = require('request-promise')
const dateFormat = require('dateformat')
const fs = require('fs')
const puppeteer = require('puppeteer');
const Audic = require("audic")

const domain = 'programare.vaccinare-covid.gov.ro'

let rawdata = fs.readFileSync('config.json')
let config = JSON.parse(rawdata)

let player = new Audic('marian.mp3')

var browser
var page

const options = {
    method: 'POST',
    uri: 'https://programare.vaccinare-covid.gov.ro/scheduling/api/centres',
    headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json, text/plain, */*'
    },
    body: {
        countyID: config.judet,
        identificationCode: config.cnp,
        masterPersonnelCategoryID: -4,
        personnelCategoryID: 32,
        recipientID: config.recipientID
    },
    json: true
}

async function getCookies() {
    if (options.headers['Cookie']) {
        checkSlots()
        return
    }

    let allCookiesContainer = await page._client.send('Network.getAllCookies')
    let sessionCookie = allCookiesContainer.cookies.filter(cookie => cookie.domain == domain && cookie.name == 'SESSION' && cookie.path =='/')[0]
    
    if (sessionCookie) {
        console.log('Gata, m-am logat, verifik...')
        let cookie = 'SESSION=' + sessionCookie.value
        options.headers['Cookie'] = cookie
        checkSlots()
    } else {
        console.log('Stau după session cookie...')
    }
}

function checkSlots() {
    request(options)
    .then(function (response) {

        if (browser) {
            browser.close()
            page = null
            browser = null
        }

        console.log('Locuri Libere ' + dateFormat(new Date(), "yyyy-mm-dd h:MM:ss") + ':\n--------------------------------------')
        let slots = response.content.map(location => ({ available: location.availableSlots, city: location.localityName, center: location.name })).filter(slot => slot.available > 0)
        
        if (slots.length > 0) {
            console.log(slots)
            player.play()
        } else {
            console.log("N-am!")
        }
        console.log("\n")
    })
    .catch(function (error) {
        console.log("N-ai tu dialog cu mine! Stai să mă loghez iară...")
        options.headers['Cookie'] = null
        login()
    })
}

async function login() {
    browser = await puppeteer.launch({
        headless: true  
    })

    page = await browser.newPage()

    await page.goto('https://programare.vaccinare-covid.gov.ro/auth/login', { waitUntil: 'networkidle0' })

    await page.type('#mat-input-0', config.user)
    await page.type('#mat-input-1', config.pass)

    await Promise.all([
        await page.click('#mat-tab-content-0-0 > div > app-authenticate-with-email > form > button'),
        await page.waitForNavigation()
    ])
}

console.log('Stai numa puțin să mă loghez...')
login()
setInterval(getCookies, 25000)